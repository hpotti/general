# # Command line instructions
# Git global setup

``` 
git config --global user.name "Harish Potti"
git config --global user.email "harish.potti@cern.ch"
```

# Create a new repository

```
git clone https://:@gitlab.cern.ch:8443/hpotti/general.git
cd general
touch README.md
git add README.md
git commit -m "add README"
git push -u origin master
```
# Existing folder
```
cd existing_folder
git init
git remote add origin https://:@gitlab.cern.ch:8443/hpotti/general.git
git add .
git commit
git push -u origin master
```
# Existing Git repository
```
cd existing_repo
git remote add origin https://:@gitlab.cern.ch:8443/hpotti/general.git
git push -u origin --all
git push -u origin --tags
```