# Author: Harish Potti
# Cutflow comparisons table for ttbar and ttH samples
# Copied from Run2Plotting software

import ROOT, yaml, subprocess

lumi=36075.0

def get_xsection(ntuple):
    cmd = "source /code/harish/cflow_check/xsec.sh %s" %ntuple
    proc = subprocess.Popen(cmd, stdout = subprocess.PIPE, shell=True)
    return float(proc.stdout.read())



def get_yield(ntuple, cut):
    f = ROOT.TFile().Open(ntuple)
    nseen = f.Get("loose/Count").GetBinContent(2)
    xsec = get_xsection(ntuple)
    #print nseen, lumi, xsec
    scale_weight = xsec*lumi/nseen
    weight = "(%.10f*mcWeightOrg*pileupEventWeight_090*MV2c10_70_EventWeight*lepSFObjLoose*lepSFTrigLoose*tauSFTight*JVT_EventWeight*SherpaNJetWeight)" %(scale_weight)
    x = "lep_Pt_0/1e3"
    htmp1 = ROOT.TH1F("htmp1",x, 100,0,100)
    #print weight
    args = ("%s>>htmp1" %x, (weight+("*(%s)" % cut if cut else "")), "e,goff")
    #print args
    f.nominal.Draw(*args)
    error = ROOT.Double()
    intg = htmp1.IntegralAndError(0, htmp1.GetNbinsX()+1, error)
    return intg, error




path="/data_ceph/onyisi/25ns_v27/01/Nominal/"
#files=["410009","410189","410503","410252","410527","410121","410505","410120","410504","410528"]
files = ["343365","343366","343367"]
rv = []
inf="/code/harish/cflow_check/4l_iso.yaml"
ouf=open("output_extratth.txt",'w')
doc = yaml.safe_load(open(inf))
c1=ROOT.TCanvas()
#print doc
#for dsid in files:
if 1:
    #print p['name'], p['cut']
    print >> ouf, '$t\bar{t}H$ ',
    for p in doc:
        total = 0
        terror = 0
        for dsid in files:
            intg, error = get_yield(path+dsid+".root",p['cut'])
            #print p['name'], '%.3f,  %.3f' % (intg, error)
            #print >> ouf, '& %.2f $\pm$ %.2f' % (intg, error),
            total += intg
            terror += error
        print >> ouf, '& %.2f $\pm$ %.2f' % (total, terror),
        #print >> ouf, r'\\'
    
        

    



