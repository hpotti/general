REG="LjZpeak"
VARIABLES=( "nJets_OR_T"
	    "nBJets"
	    "lep_Pt_0"
	    "lep_Pt_1"
	    "lep_Pt_2"
	    "DRll01"
	    "DRll02"
	    "DRlj00"
	    "DPhij0MET"
	    "HT_jets"
	    "lead_jetPt"
	    "sublead_jetPt"
	    "Mll01"
	    "Mll02"
	    "Mll12"
	    "Mlll012"
	    "trilep_type-1"
	    "total_charge"
)
RANGES=( "'nJets_OR_T_overflow',7,2.5,9.5"
	 '"nJets_OR_T_MV2c10_70_overflow",5,0.5,5.5'
	 '"lep_Pt_0/1000.",19,10,200'
	 '"lep_Pt_1/1000.",19,10,200'
	 '"lep_Pt_2/1000.",19,10,200'
	 '"DRll01",14,0,3.5'
	 '"DRll02",16,0.5,4.5'
	 '"DRlj00",18,0.25,4.75'
	 '"DPhij0MET",20,0,3'
	 '"HT_jets/1000.",19,50,1000'
	 '"lead_jetPt/1000.",28,25,305'
	 '"sublead_jetPt/1000.",28,25,305'
	 '"Mll01/1000.",20,0,250'
	 '"Mll02/1000.",20,0,250'
	 '"Mll12/1000.",20,0,250'
	 '"Mlll012/1000.",20,0,400'
	 '"trilep_type-1",4,-0.5,3.5'
	 '"total_charge",3,-1.5,1.5'
    )
i=0
for VAR in "${VARIABLES[@]}"
do
    DETAIL=${RANGES[$i]}
    i=$(( $i + 1 ))
cat <<EOF >> myfile.txt
Region: "${REG}_${VAR}"
   Type: VALIDATION
   Variable:$DETAIL
   VariableTitle: $VAR
   Label: $REG
   Selection:"XXX_LLLNTAU_BASIC_SELECTION&&(lep_Pt_0>10e3 && lep_Pt_1>20e3 && lep_Pt_2>20e3)&& (((lep_ID_0==-lep_ID_1 && (Mll01>81.2e3 && Mll01<101.2e3)) || (lep_ID_0==-lep_ID_2 && (Mll02>81.2e3 && Mll02<101.2e3)))) && (nJets_OR_T>=4 && nJets_OR_T_MV2c10_70>=1 || nJets_OR_T==3 && nJets_OR_T_MV2c10_70 >= 2) && (nJets_OR_T<=5)"
   DataType: DATA 
   LogScale: FALSE


EOF
done
